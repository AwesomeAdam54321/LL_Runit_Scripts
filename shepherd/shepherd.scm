;; shepherd.scm -- Personal shepherd configuration of Adam Faiz.
;; Copyright © 2022, 2023 Adam Faiz <adam.faiz@disroot.org>
;;
;; This GNU Shepherd configuration is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This GNU Shepherd configuration is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.



;; Notes about my config: I switched the init system of my GNU/Linux distro
;; from systemd to runit, then to shepherd using the GOOPS syntax,
;; then updated it to the new shepherd config syntax.

;; I hope this config shows what the GNU Shepherd is capable of, and that
;; switching init systems isn't actually as complicated as you may think.



;; init.scm -- default shepherd configuration file.

;; Services known to shepherd:
;; Add new services (defined using "(service '(<service>)") to shepherd here by
;; providing them as arguments to 'register-services'.

(define yacy
  (service '(yacy)
	   #:requirement '()
	   #:documentation "YaCy is free software for your own search engine."
	   #:start (make-forkexec-constructor '("./startYACY.sh" "--foreground")
					      #:user "adam" #:group "adam" #:directory "/home/adam/Downloads/yacy")
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define dovecot
  (service '(dovecot)
	   #:requirement '()
	   #:documentation "Dovecot is a free software IMAP and POP3 server for GNU."
	   #:start (make-forkexec-constructor '("dovecot" "-F"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define getty@tty2
  (service '(getty@tty2)
	   #:requirement '()
	   #:documentation "The getty@tty2 service provides a getty on tty2."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/getty@tty2"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define cron
  (service '(cron)
	   #:requirement '()
	   #:documentation "The cron service provides execution of regularly scheduled commands."
	   #:start (make-forkexec-constructor '("cron" "-f"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define accounts-daemon
  (service '(accounts-daemon)
	   #:requirement '(dbus)
	   #:documentation "The accounts-daemon provides the Accounts Service."
	   #:start (make-forkexec-constructor '("env" "GVFS_DISABLE_FUSE=1" "GIO_USE_VFS=local" "GVFS_REMOTE_VOLUME_MONITOR_IGNORE=1"
						"/usr/lib/accountsservice/accounts-daemon"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define apparmor
  (service '(apparmor)
	   #:requirement '()
	   #:documentation "Loads AppArmor profiles."
	   #:start (make-forkexec-constructor '("/lib/apparmor/apparmor.systemd" "reload"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define avahi-daemon
  (service '(avahi-daemon)
	   #:requirement '(dbus)
	   #:documentation "The avahi-daemon provides the Avahi mDNS/DNS-SD Stack."
	   #:start (make-forkexec-constructor '("avahi-daemon" "-s"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define binfmt-support
  (service '(binfmt-support)
	   #:requirement '()
	   #:documentation "Enables support for additional executable binary formats."
	   #:start (make-forkexec-constructor '("update-binfmts" "--enable"))
	   #:stop (make-system-destructor "update-binfmts --disable")
	   #:one-shot? #t))

(define casper
  (service '(casper)
	   #:requirement '()
	   #:documentation "Shuts down the 'live' preinstalled system cleanly."
	   #:start (make-forkexec-constructor '("casper-stop"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define colord
  (service '(colord)
	   #:requirement '(dbus)
	   #:documentation "The colord service is used to manage, install generate Colour Profiles."
	   #:start (make-forkexec-constructor '("/usr/libexec/colord") #:user "colord")
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define console-setup
  (service '(console-setup)
	   #:requirement '()
	   #:documentation "Sets the system console font and keymap."
	   #:start (make-forkexec-constructor '("/lib/console-setup/console-setup.sh"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define cups
  (service '(cups)
	   #:requirement '()
	   #:documentation "The cups service provides the CUPS scheduler."
	   #:start (make-forkexec-constructor '("cupsd" "-f"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define cups-browsed
  (service '(cups-browsed)
	   #:requirement '(cups)
	   #:documentation "The cups-browsed service makes remote CUPS printers available locally."
	   #:start (make-forkexec-constructor '("cups-browsed"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define dbus
  (service '(dbus)
	   #:requirement '()
	   #:documentation "The dbus service provides the D-Bus System Message Bus."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/dbus"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define lightdm
  (service '(lightdm display-manager)
	   #:requirement '(dbus)
	   #:documentation "The lightdm service provides the Light Display Manager."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/lightdm"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define dm-event
  (service '(dm-event)
	   #:requirement '()
	   #:documentation "The dm-event service provides an event monitoring daemon for device-mapper devices."
	   #:start (make-forkexec-constructor '("env" "SD_ACTIVATION=1" "dmeventd" "-f"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define dns-clean
  (service '(dns-clean)
	   #:requirement '()
	   #:documentation "Clean up any mess left by 0dns-up"
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/dns-clean"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define firewalld
  (service '(firewalld)
	   #:requirement '(dbus polkit)
	   #:documentation "The firewalld service provides a dynamically managed firewall."
	   #:start (make-forkexec-constructor '("firewalld" "--nofork" "--nopid"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define grub-common
  (service '(grub-common)
	   #:requirement '()
	   #:documentation "Record successful boot for GRUB."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/grub-common"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define grub-initrd-fallback
  (service '(grub-initrd-fallback)
	   #:requirement '(grub-common)
	   #:documentation "GRUB failed boot detection."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/grub-initrd-fallback"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define guix-daemon
  (service '(guix-daemon)
	   #:requirement '()
	   #:documentation "The guix-daemon service accesses the store, and builds derivations on behalf of its clients."
	   #:start (make-forkexec-constructor '("env" "GUIX_LOCPATH=/var/guix/profiles/per-user/root/guix-profile/lib/locale" "LC_ALL=en_US.utf8" "/var/guix/profiles/per-user/root/current-guix/bin/guix-daemon" "--build-users-group=guixbuild" "--discover=yes" "--substitute-urls=https://bordeaux-singapore-mirror.cbaines.net/ https://bordeaux.guix.gnu.org https://ci.guix.gnu.org"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define ifupdown-pre
  (service '(ifupdown-pre)
	   #:requirement '()
	   #:documentation "Helper to synchronize boot up for ifupdown."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/ifupdown-pre"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define ifupdown-wait-online
  (service '(ifupdown-wait-online)
	   #:requirement '()
	   #:documentation "Wait for network to be configured by ifupdown."
	   #:start (make-forkexec-constructor '("/lib/ifupdown/wait-online.sh"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define initrd-udevadm-cleanup-db
  (service '(initrd-udevadm-cleanup-db)
	   #:requirement '(systemd-udevd systemd-udev-trigger systemd-udev-settle)
	   #:documentation "Cleanup udevd DB."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/initrd-udevadm-cleanup-db"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define irqbalance
  (service '(irqbalance)
	   #:requirement '()
	   #:documentation "The irqbalance service distributes hardware interrupts across processors on a multiprocessor system."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/irqbalance"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define keyboard-setup
  (service '(keyboard-setup)
	   #:requirement '()
	   #:documentation "Set the console keyboard layout."
	   #:start (make-forkexec-constructor '("/lib/console-setup/keyboard-setup.sh"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define kmod
  (service '(kmod)
	   #:requirement '()
	   #:documentation "Load Kernel Modules."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/kmod"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define kmod-static-nodes
  (service '(kmod-static-nodes)
	   #:requirement '()
	   #:documentation "Create list of static device nodes for the current kernel."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/kmod-static-nodes"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define lm-sensors
  (service '(lm-sensors)
	   #:requirement '()
	   #:documentation "Initialize hardware monitoring sensors."
	   #:start (make-system-constructor "sensors -s ; sensors")
	   #:stop (make-system-destructor)
	   #:one-shot? #t))

(define lvm2-lvmpolld
  (service '(lvm2-lvmpolld)
	   #:requirement '()
	   #:documentation "The lvm2-lvmpolld service provides the LVM2 poll daemon."
	   #:start (make-forkexec-constructor '("env" "SD_ACTIVATION=1"  "lvmpolld" "-t" "60" "-f"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define lvm2-monitor
  (service '(lvm2-monitor)
	   #:requirement '()
	   #:documentation "Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling."
	   #:start (make-forkexec-constructor '("env" "LVM_SUPPRESS_LOCKING_FAILURE_MESSAGES=1" "lvm" "vgchange" "--monitor" "y"))
	   #:stop (make-system-destructor "lvm vgchange --monitor n")
	   #:one-shot? #t))

(define ModemManager
  (service '(ModemManager)
	   #:requirement '(dbus polkit)
	   #:documentation "The ModemManager service provides mobile broadband modem management."
	   #:start (make-forkexec-constructor '("ModemManager" "--filter-policy=strict"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define networkd-dispatcher
  (service '(networkd-dispatcher)
	   #:requirement '()
	   #:documentation "The networkd-dispatcher service provides the Dispatcher daemon for systemd-networkd."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/networkd-dispatcher"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define networking
  (service '(networking)
	   #:requirement '(ifupdown-pre)
	   #:documentation "Raise network interfaces."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/networking"))
	   #:stop (make-system-destructor "[ -r /etc/default/networking ] && . /etc/default/networking; exec ifdown -a --read-environment --exclude=lo")
	   #:one-shot? #t))

(define NetworkManager
  (service '(NetworkManager network-manager)
	   #:requirement '(dbus)
	   #:documentation "The NetworkManager service provides an interface to network management."
	   #:start (make-forkexec-constructor '("NetworkManager" "--no-daemon"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define NetworkManager-dispatcher
  (service '(NetworkManager-dispatcher)
	   #:requirement '(dbus)
	   #:documentation "The NetworkManager-dispatcher service provides the Network Manager Script Dispatcher."
	   #:start (make-forkexec-constructor '("/usr/lib/NetworkManager/nm-dispatcher" "--persist"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define NetworkManager-wait-online
  (service '(NetworkManager-wait-online)
	   #:requirement '(NetworkManager)
	   #:documentation "Network Manager Wait Online."
	   #:start (make-forkexec-constructor '("nm-online" "-s" "-q" "--timeout=30"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define nscd
  (service '(nscd)
	   #:requirement '()
	   #:documentation "The nscd service provides name service caching to requesting programs."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/nscd"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define ntp
  (service '(ntp)
	   #:requirement '(networking)
	   #:documentation "The ntp service synchronises the network time on the system."
	   #:start (make-forkexec-constructor '("ntpd" "--nofork" "-g"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define ondemand
  (service '(ondemand)
	   #:requirement '()
	   #:documentation "Set the CPU Frequency Scaling governor."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/ondemand"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define plymouth-halt
  (service '(plymouth-halt plymouth-kexec plymouth-poweroff)
	   #:requirement '()
	   #:documentation "The plymouth-halt service shows the Plymouth Halt Screen."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/plymouth-halt"))
	   #:stop (make-system-destructor "plymouth show-splash")
	   #:respawn? #t))

(define plymouth-quit
  (service '(plymouth-quit plymouth)
	   #:requirement '()
	   #:documentation "Terminate Plymouth Boot Screen."
	   #:start (make-forkexec-constructor '("plymouth" "quit"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define plymouth-quit-wait
  (service '(plymouth-quit-wait)
	   #:requirement '()
	   #:documentation "Hold until boot process finishes up."
	   #:start (make-forkexec-constructor '("plymouth" "--wait"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define plymouth-read-write
  (service '(plymouth-read-write)
	   #:requirement '()
	   #:documentation "Tell Plymouth To Write Out Runtime Data."
	   #:start (make-system-constructor "test ! -e /etc/initrd-release && plymouth update-root-fs --read-write")
	   #:stop (make-system-destructor)
	   #:one-shot? #t))

(define plymouth-reboot
  (service '(plymouth-reboot)
	   #:requirement '()
	   #:documentation "The plymouth-reboot service shows the Plymouth Reboot Screen."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/plymouth-reboot"))
	   #:stop (make-system-destructor "plymouth show-splash")
	   #:respawn? #t))

(define plymouth-start
  (service '(plymouth-start)
	   #:requirement '()
	   #:documentation "The plymouth-reboot service shows the Plymouth Boot Screen."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/plymouth-start"))
	   #:stop (make-system-destructor "plymouth show-splash")
	   #:respawn? #t))

(define plymouth-switch-root
  (service '(plymouth-switch-root)
	   #:requirement '()
	   #:documentation "Plymouth switch root service."
	   #:start (make-system-constructor "test -e /etc/initrd-release && plymouth update-root-fs --new-root-dir=/sysroot")
	   #:stop (make-system-destructor)
	   #:one-shot? #t))

(define polkit
  (service '(polkit)
	   #:requirement '(dbus)
	   #:documentation "The polkit service provides PolicyKit, an Authorization Manager."
	   #:start (make-forkexec-constructor '("/usr/lib/policykit-1/polkitd" "--no-debug"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define pppd-dns
  (service '(pppd-dns)
	   #:requirement '()
	   #:documentation "Restore /etc/resolv.conf if the system crashed before the ppp link was shut down."
	   #:start (make-forkexec-constructor '("/etc/ppp/ip-down.d/0000usepeerdns"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define pulseaudio
  (service '(pulseaudio)
	   #:requirement '()
	   #:documentation "The pulseaudio service provides the PulseAudio Sound Server."
	   #:start (make-forkexec-constructor '("pulseaudio" "--daemonize=no" "--log-target=stderr") #:user "adam" #:group "audio" #:environment-variables '("HOME=/home/adam"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define resolvconf
  (service '(resolvconf)
	   #:requirement '()
	   #:documentation "Nameserver information manager."
	   #:start (make-forkexec-constructor '("resolvconf" "--enable-updates"))
	   #:stop (make-system-destructor "resolvconf --disable-updates")
	   #:one-shot? #t))

(define resolvconf-pull-resolved
  (service '(resolvconf-pull-resolved)
	   #:requirement '()
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/resolvconf-pull-resolved"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define rsyslog
  (service '(rsyslog syslog)
	   #:requirement '()
	   #:documentation "The rsyslog service provides System Logging."
	   #:start (make-forkexec-constructor '("chpst" "-o" "16384" "rsyslogd" "-n" "-iNONE"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define rtkit-daemon
  (service '(rtkit-daemon)
	   #:requirement '(dbus)
	   #:documentation "The rtkit-daemon provides the RealtimeKit Scheduling Policy."
	   #:start (make-forkexec-constructor '("/usr/libexec/rtkit-daemon"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define smartmontools
  (service '(smartmontools smartd)
	   #:requirement '()
	   #:documentation "The smartmontools service provides Self Monitoring and Reporting Technology (SMART)."
	   #:start (make-forkexec-constructor '("/usr/local/etc/init.d/smartmontools"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define systemd-udevd
  (service '(systemd-udevd)
	   #:requirement '()
	   #:documentation "The systemd-udevd service provides systemd's udev Kernel Device Manager."
	   #:start (make-forkexec-constructor '("/lib/systemd/systemd-udevd"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define systemd-udev-settle
  (service '(systemd-udev-settle)
	   #:requirement '()
	   #:documentation "udev Wait for Complete Device Initialization."
	   #:start (make-forkexec-constructor '("udevadm" "settle"))
	   #:stop (make-kill-destructor)
	   #:one-shot? #t))

(define systemd-udev-trigger
  (service '(systemd-udev-trigger)
	   #:requirement '()
	   #:documentation "udev Coldplug all Devices."
	   #:start (make-system-constructor "udevadm trigger --type=subsystems --action=add && udevadm trigger --type=devices --action=add")
	   #:stop (make-system-destructor)
	   #:one-shot? #t))

(define tlp
  (service '(tlp)
	   #:requirement '()
	   #:documentation "TLP system startup/shutdown."
	   #:start (make-forkexec-constructor '("tlp" "init" "start"))
	   #:stop (make-system-destructor "tlp init stop")
	   #:one-shot? #t))

(define wpa-supplicant
  (service '(wpa-supplicant)
	   #:requirement '(dbus)
	   #:documentation "The wpa_supplicant service provides the WPA supplicant."
	   #:start (make-forkexec-constructor '("wpa_supplicant" "-u" "-s" "-O" "/run/wpa_supplicant"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(define thermald
  (service '(thermald)
	   #:requirement '()
	   #:documentation "thermald is a daemon used to prevent the overheating of platforms."
	   #:start (make-forkexec-constructor '("thermald" "--ignore-cpuid-check" "--adaptive" "--no-daemon" "--poll-interval" "0"))
	   #:stop (make-kill-destructor)
	   #:respawn? #t))

(register-services (list accounts-daemon
			 apparmor
			 avahi-daemon
			 binfmt-support
			 casper
			 colord
			 console-setup
			 cron
			 cups
			 cups-browsed
			 dbus
			 dm-event
			 dns-clean
			 firewalld
			 getty@tty2
			 grub-common
			 grub-initrd-fallback
			 guix-daemon
			 ifupdown-pre
			 ifupdown-wait-online
			 initrd-udevadm-cleanup-db
			 irqbalance keyboard-setup
			 kmod
			 kmod-static-nodes
			 lightdm
			 lm-sensors
			 ModemManager
			 networking
			 NetworkManager
			 NetworkManager-dispatcher
			 NetworkManager-wait-online
			 nscd
			 ntp
			 ondemand
			 plymouth-read-write
			 plymouth-switch-root
			 plymouth-start
			 polkit
			 pppd-dns
			 pulseaudio
			 rsyslog
			 rtkit-daemon
			 smartmontools
			 systemd-udev-settle
			 systemd-udev-trigger
			 systemd-udevd
			 tlp
			 wpa-supplicant))

;; Services to start when shepherd starts:
;; Add the name of each service that should be started to the list
;; below passed to 'for-each'.

(for-each start-service (list accounts-daemon
			      apparmor
			      avahi-daemon
			      binfmt-support
			      casper
			      colord
			      console-setup
			      cron
			      cups
			      cups-browsed
			      dbus
			      dm-event
			      dns-clean
			      firewalld
			      getty@tty2
			      grub-common
			      grub-initrd-fallback
			      guix-daemon
			      ifupdown-pre
			      ifupdown-wait-online
			      initrd-udevadm-cleanup-db
			      irqbalance keyboard-setup
			      kmod
			      kmod-static-nodes
			      lightdm
			      lm-sensors
			      ModemManager
			      networking
			      NetworkManager
			      NetworkManager-dispatcher
			      NetworkManager-wait-online
			      nscd
			      ntp
			      ondemand
			      plymouth-read-write
			      plymouth-switch-root
			      plymouth-start
			      polkit
			      pppd-dns
			      pulseaudio
			      rsyslog
			      rtkit-daemon
			      smartmontools
			      systemd-udev-settle
			      systemd-udev-trigger
			      systemd-udevd
			      tlp
			      wpa-supplicant))
