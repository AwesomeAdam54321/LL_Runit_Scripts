# LL_Runit_Scripts

This repo contains runit services that I made based on systemd services in Linux Lite GNU/Linux. They should work on other GNU/Linux distributions as well.

There are instructions on how to replace systemd init with runit-init or the GNU Shepherd in the instructions.txt.

Nowadays, I mainly use the GNU Shepherd as my init system, and the configuration reuses some of my runit config.
The instructions are in the shepherd/ directory.

# License
Everything in this repo is licensed under the GPLv3 or later.